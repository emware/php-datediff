<?php
declare(strict_types=1);

namespace Subito\Interfaces;

interface DateDiffInterface
{
    public function getYears(): int;

    public function getMonths(): int;

    public function getDays(): int;

    public function getTotalDays(): int;

    public function isInverse(): bool;
}