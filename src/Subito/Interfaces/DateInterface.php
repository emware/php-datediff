<?php
declare(strict_types=1);

namespace Subito\Interfaces;

use Subito\Models\Date;

interface DateInterface
{
    public function getYear(): int;

    public function getMonth(): int;

    public function getDay(): int;
}