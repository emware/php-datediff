<?php
declare(strict_types=1);

namespace Subito\Models;

use InvalidArgumentException;
use Subito\Interfaces\DateInterface;
use Subito\Utility\GregorianCalendar;

class Date implements DateInterface
{
    private $year;
    private $month;
    private $day;

    public function __construct(int $year, int $month, int $day)
    {
        $this->year = $year;
        $this->month = $month;
        $this->day = $day;
    }

    public function getYear(): int
    {
        return $this->year;
    }

    public function getMonth(): int
    {
        return $this->month;
    }

    public function getDay(): int
    {
        return $this->day;
    }

    public function __toString(): string
    {
        return sprintf('%s/%s/%s',
            $this->getYear(),
            str_pad((string) $this->getMonth(), 2, '0', STR_PAD_LEFT),
            str_pad((string) $this->getDay(), 2, '0', STR_PAD_LEFT)
        );
    }
}