<?php
declare(strict_types=1);

namespace Subito\Models;

use Subito\Interfaces\DateDiffInterface;

class DateDiff implements DateDiffInterface
{
    /**
     * @var int
     */
    private $years;

    /**
     * @var int
     */
    private $months;

    /**
     * @var int
     */
    private $days;

    /**
     * @var int
     */
    private $totalDays;

    /**
     * @var bool
     */
    private $inverse;

    public function __construct(
        int $years,
        int $months,
        int $days,
        int $totalDays,
        bool $inverse
    ) {
        $this->years = $years;
        $this->months = $months;
        $this->days = $days;
        $this->totalDays = $totalDays;
        $this->inverse = $inverse;
    }

    /**
     * @return int
     */
    public function getYears(): int
    {
        return $this->years;
    }

    /**
     * @return int
     */
    public function getMonths(): int
    {
        return $this->months;
    }

    /**
     * @return int
     */
    public function getDays(): int
    {
        return $this->days;
    }

    /**
     * @return int
     */
    public function getTotalDays(): int
    {
        return $this->totalDays;
    }

    /**
     * @return bool
     */
    public function isInverse(): bool
    {
        return $this->inverse;
    }
}