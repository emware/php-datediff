<?php
declare(strict_types=1);

namespace Subito\Utility;

use Subito\Models\Date;

class DateFactory
{
    /**
     * @var GregorianCalendar
     */
    private $calendar;

    public function __construct(GregorianCalendar $calendar)
    {
        $this->calendar = $calendar;
    }

    public function createFromString(string $date): ?Date
    {
        $matches = [];
        preg_match("/^(\d{4})\/(0[1-9]|1[0-2])\/(0[1-9]|1\d|2\d|3[0-1])$/", $date, $matches);

        if (0 === count($matches)) {
            return null;
        }

        [$year, $month, $day] = array_slice($matches, 1, 3);

        return $this->createFromValues((int) $year, (int) $month, (int) $day);
    }

    public function createFromValues(int $year, int $month, int $day): ?Date
    {
        if ($day > $this->calendar->getDaysInMonth($month, $this->calendar->isLeapYear($year))) {
            return null;
        }

        return new Date($year, $month, $day);
    }
}