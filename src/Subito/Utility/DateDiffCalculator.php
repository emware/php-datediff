<?php
declare(strict_types=1);

namespace Subito\Utility;

use Exception;
use SebastianBergmann\Diff\Diff;
use Subito\Models\Date;
use Subito\Models\DateDiff;

class DateDiffCalculator
{
    /**
     * @var GregorianCalendar
     */
    private $calendar;

    public function __construct(GregorianCalendar $calendar)
    {
        $this->calendar = $calendar;
    }

    public function diff(Date $fromDate, Date $toDate): DateDiff
    {
        $invert = (string) $fromDate > (string) $toDate;

        if ($invert) {
            [$fromDate, $toDate] = [$toDate, $fromDate];
        }

        return array_reduce(
            range($fromDate->getYear(), $toDate->getYear()),
            static::calculateYearDateDiff($fromDate, $toDate, $this->calendar),
            new DateDiff(0, 0, 0, 0, $invert)
        );
    }

    private static function calculateYearDateDiff(Date $fromDate, Date $toDate, GregorianCalendar $calendar): callable
    {
        return static function (DateDiff $diff, int $currentYear) use ($fromDate, $toDate, $calendar): DateDiff {
            $fromMonth = $currentYear === $fromDate->getYear() ? $fromDate->getMonth() : 1;
            $toMonth = $currentYear === $toDate->getYear() ? $toDate->getMonth() : 12;

            return array_reduce(
                range($fromMonth, $toMonth),
                static::calculateMonthDateDiff($currentYear, $fromDate, $toDate, $calendar),
                $diff
            );
        };
    }

    private static function getFirstDayOfDiffPeriod(int $currentYear, int $currentMonth, Date $fromDate): int
    {
        // We start counting from 0 to count the first day of month as active part of the diff
        return ($currentYear === $fromDate->getYear() && $currentMonth === $fromDate->getMonth())
            ? $fromDate->getDay()
            : 0;
    }

    private static function getLastDayOfDiffPeriod(
        int $currentYear,
        int $currentMonth,
        Date $toDate,
        GregorianCalendar $calendar
    ): int {
        return ($currentYear === $toDate->getYear() && $currentMonth === $toDate->getMonth())
            ? $toDate->getDay()
            : $calendar->getDaysInMonth($currentMonth, $calendar->isLeapYear($currentYear));
    }

    private static function calculateDayDiff(
        int $currentYear,
        int $currentMonth,
        Date $fromDate,
        Date $toDate,
        GregorianCalendar $calendar
    ): int {
        return
            static::getLastDayOfDiffPeriod($currentYear, $currentMonth, $toDate, $calendar) -
            static::getFirstDayOfDiffPeriod($currentYear, $currentMonth, $fromDate);
    }

    private static function incrementDaysOfDiff(DateDiff $diff, int $diffDaysToAdd, int $numberOfDayInMonth): DateDiff
    {
        $totalDayCount = $diff->getDays() + $diffDaysToAdd;

        return $totalDayCount >= $numberOfDayInMonth
            ? new DateDiff(
                $diff->getYears(),
                $diff->getMonths() + 1,
                $totalDayCount -= $numberOfDayInMonth,
                $diff->getTotalDays() + $diffDaysToAdd,
                $diff->isInverse()
            )
            : new DateDiff(
                $diff->getYears(),
                $diff->getMonths(),
                $diff->getDays() + $diffDaysToAdd,
                $diff->getTotalDays() + $diffDaysToAdd,
                $diff->isInverse()
            );
    }

    private static function incrementYearsOfDiff(DateDiff $diff): DateDiff
    {
        return $diff->getMonths() < 12
            ? $diff
            : new DateDiff(
                $diff->getYears() + 1,
                0,
                $diff->getDays(),
                $diff->getTotalDays(),
                $diff->isInverse()
            );
    }

    private static function calculateMonthDateDiff(
        int $currentYear,
        Date $fromDate,
        Date $toDate,
        GregorianCalendar $calendar
    ): callable {
        return static function(
            DateDiff $diff,
            int $currentMonth
        ) use ($currentYear, $fromDate, $toDate, $calendar): DateDiff
        {
            $dayToAdd = static::calculateDayDiff($currentYear, $currentMonth, $fromDate, $toDate, $calendar);

            $numberOfDayInMonth = $calendar->getDaysInMonth($currentMonth, $calendar->isLeapYear($currentYear));

            $diff = static::incrementDaysOfDiff($diff, $dayToAdd, $numberOfDayInMonth);

            return static::incrementYearsOfDiff($diff);
        };
    }
}
