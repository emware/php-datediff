<?php
declare(strict_types=1);

namespace Subito\Utility;

use Subito\Models\Date;

class GregorianCalendar
{
    private static $calendar = [
        1 => 31,
        2 => 28,
        3 => 31,
        4 => 30,
        5 => 31,
        6 => 30,
        7 => 31,
        8 => 31,
        9 => 30,
        10 => 31,
        11 => 30,
        12 => 31,
    ];

    private static $leapCalendar = [];

    public function __construct()
    {
        self::$leapCalendar = self::$calendar;
        self::$leapCalendar[2] = 29;
    }

    public function isLeapYear(int $year): bool
    {
        if ($year % 400 === 0) {
            return true;
        }

        if ($year % 100 === 0) {
            return false;
        }

        return $year % 4 === 0;
    }

    public function getDaysInMonth(int $month, bool $isLeapYear): int
    {
        return $isLeapYear ? self::$leapCalendar[$month] : self::$calendar[$month];
    }

    public function getPreviousMonthNumber(int $startMonth): int
    {
        return $startMonth === 1 ? 12 : --$startMonth;
    }
}