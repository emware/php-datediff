<?php

require_once __DIR__ . '/../vendor/autoload.php';


$totalArguments = count($argv);

if (count($argv) !== 3) {
    echo "----------\n";
    echo "WRONG USAGE:\n";
    echo "Use Example: php bin/diff.php YYYY/MM/DD YYYY/MM/DD\n";
    echo "----------\n";

    return;
}

$calendar = new \Subito\Utility\GregorianCalendar();
$factory = new \Subito\Utility\DateFactory($calendar);
$fromDate = $factory->createFromString($argv[1]);
$toDate = $factory->createFromString($argv[2]);

if ( null === $fromDate || null === $toDate) {
    echo "----------\n";
    echo "INVALID DATE FORMAT:\n";
    echo "Use Example: php bin/diff.php YYYY/MM/DD YYYY/MM/DD\n";
    echo "----------\n";

    return;
}

$diffCalculator = new \Subito\Utility\DateDiffCalculator($calendar);

var_dump($diffCalculator->diff($fromDate, $toDate));
