<?php
declare(strict_types=1);

namespace Unit\Utility;

use Subito\Models\Date;
use Subito\Utility\DateFactory;
use PHPUnit\Framework\TestCase;
use Subito\Utility\GregorianCalendar;

class DateFactoryTest extends TestCase
{
    /**
     * @dataProvider validDateDateProvider
     * @param string $date
     */
    public function testIsValidDate(string $date): void {
        $calendar = new GregorianCalendar();
        $factory = new DateFactory($calendar);

        $this->assertInstanceOf(Date::class, $factory->createFromString($date));
    }

    /**
     * @dataProvider invalidDateDateProvider
     * @param string $date
     */
    public function testIsNotValidDate(string $date): void {
        $calendar = new GregorianCalendar();
        $factory = new DateFactory($calendar);

        $this->assertNull($factory->createFromString($date));
    }

    public function validDateDateProvider(): array
    {
        return [
            ['2015/12/25'],
            ['2015/01/01'],
            ['2015/12/01'],
            ['2015/01/31'],
            ['2015/02/28'],
            ['2016/02/29'],
            ['0000/01/01'],
        ];
    }

    public function invalidDateDateProvider(): array
    {
        return [
            ['FAKE/12/25'],
            ['2015/FA/KE'],
            ['2015/13/01'],
            ['2015/01/32'],
            ['2015/02/29'],
            ['2016/02/30'],
            ['0000/00/00'],
        ];
    }
}
