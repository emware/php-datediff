<?php
declare(strict_types=1);

namespace Subito\Tests\Unit\Models;

use Subito\Models\Date;
use PHPUnit\Framework\TestCase;
use Subito\Utility\GregorianCalendar;

class GregorianCalendarTest extends TestCase
{
    public function testIsLeapYear(): void
    {
        $gregorianCalendar = new GregorianCalendar();

        $this->assertTrue($gregorianCalendar->isLeapYear(0000));
        $this->assertFalse($gregorianCalendar->isLeapYear(2015));
        $this->assertTrue($gregorianCalendar->isLeapYear(2016));
        $this->assertTrue($gregorianCalendar->isLeapYear(400));
        $this->assertFalse($gregorianCalendar->isLeapYear(600));
    }

    public function testGetPreviousMonthNumber(): void
    {
        $gregorianCalendar = new GregorianCalendar();

        $this->assertSame(11, $gregorianCalendar->getPreviousMonthNumber(12));
        $this->assertSame(1, $gregorianCalendar->getPreviousMonthNumber(2));
        $this->assertSame(12, $gregorianCalendar->getPreviousMonthNumber(1));
    }
}
