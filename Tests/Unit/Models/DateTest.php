<?php
declare(strict_types=1);

namespace Subito\Tests\Unit\Models;

use Subito\Models\Date;
use PHPUnit\Framework\TestCase;
use Subito\Utility\GregorianCalendar;

class DateTest extends TestCase
{
    public function testContract(): void
    {
        $date = new Date(2019, 10, 10);

        $this->assertSame(2019, $date->getYear());
        $this->assertSame(10, $date->getMonth());
        $this->assertSame(10, $date->getDay());
    }

    public function testCompare(): void
    {
        $date = new Date(2019, 02, 05);
        $previousDate = new Date(2019, 01, 06);

        $this->assertTrue((string) $date > (string) $previousDate);
    }
}
