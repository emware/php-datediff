<?php
declare(strict_types=1);

namespace Subito\Tests\Integration\Utility;

use Faker\Factory;
use Subito\Models\Date;
use Subito\Utility\DateDiffCalculator;
use PHPUnit\Framework\TestCase;
use Subito\Utility\GregorianCalendar;

class DateDiffCalculatorTest extends TestCase
{
    /**
     * @param string $startDate
     * @param string $endDate
     *
     * @dataProvider datesDataProvider
     */
    public function testDiff(string $startDate, string $endDate): void
    {
        $startDateTest = \DateTimeImmutable::createFromFormat('Y/m/d', $startDate);
        $endDateTest = \DateTimeImmutable::createFromFormat('Y/m/d', $endDate);

        $startDateMock = $this->prophesize(Date::class);
        $endDateMock = $this->prophesize(Date::class);

        $startDate = new Date(
            (int) $startDateTest->format('Y'),
            (int) $startDateTest->format('m'),
            (int) $startDateTest->format('d')
        );

        $endDate = new Date(
            (int) $endDateTest->format('Y'),
            (int) $endDateTest->format('m'),
            (int) $endDateTest->format('d')
        );

        $diff = (new DateDiffCalculator(new GregorianCalendar()))->diff($endDate, $startDate);
        $nativeDiff = $endDateTest->diff($startDateTest);

        $this->assertSame($nativeDiff->days, $diff->getTotalDays());
        $this->assertSame($nativeDiff->invert, (int) $diff->isInverse());
        $this->assertSame($nativeDiff->y, $diff->getYears());
        $this->assertSame($nativeDiff->m, $diff->getMonths());
        $this->assertSame($nativeDiff->d, $diff->getDays());
    }

    public function datesDataProvider(): array
    {
        return [
            ['2017/10/31', '2018/12/25'],
            ['2015/10/31', '2016/12/25'],
            ['2015/01/28', '2015/03/27'],
            ['2015/02/28', '2019/02/29'],
            ['2015/02/28', '2020/03/01'],
            ['2020/03/10', '2020/03/05'],
            ['2020/03/05', '2020/03/10'],
        ];
    }
}
